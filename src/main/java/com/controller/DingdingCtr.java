//package com.controller;
//
//import com.alibaba.dingtalk.openapi.Env;
//import com.alibaba.dingtalk.openapi.auth.AuthHelper;
//import com.alibaba.dingtalk.openapi.user.UserHelper;
//import com.dingtalk.open.client.api.model.corp.CorpUserBaseInfo;
//import com.dingtalk.open.client.api.model.corp.CorpUserDetail;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.servlet.http.HttpServletRequest;
//import java.net.URLDecoder;
//import java.util.HashMap;
//import java.util.Map;
//
//@Controller
//@RequestMapping("/api/auth")
//public class DingdingCtr {
//
//    @RequestMapping(value = "/getPcJsConfig",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getPcJsConfig(HttpServletRequest request){
//        String urlString = request.getRequestURL().toString();
//        String queryString = request.getQueryString();
//        String queryStringEncode = null;
//        String url;
//        if (queryString != null) {
//            queryStringEncode = URLDecoder.decode(queryString);
//            url = urlString + "?" + queryStringEncode;
//        } else {
//            url = Env.PC_DOMAIN;
//        }
//        return AuthHelper.getConfig(url);
//    }
//
//    @RequestMapping(value = "/getJsConfig",method = RequestMethod.GET)
//    @ResponseBody
//    public Map<String,Object> getJsConfig(HttpServletRequest request){
//        String urlString = request.getRequestURL().toString();
//        String queryString = request.getQueryString();
//        String queryStringEncode = null;
//        String url;
//        if (queryString != null) {
//            queryStringEncode = URLDecoder.decode(queryString);
//            url = urlString + "?" + queryStringEncode;
//        } else {
//            url = Env.MOB_DOMAIN;
//        }
//        return AuthHelper.getConfig(url);
//    }
//
//    @RequestMapping(value = "/login",method = RequestMethod.GET)
//    public Map<String,Object> login(HttpServletRequest request) throws Exception{
//        String code = request.getParameter("code");
//        String access_token = AuthHelper.getAccessToken();
//        Map<String,Object> map = new HashMap<String,Object>();
//        try {
//            CorpUserBaseInfo CorpUserBaseInfo = UserHelper.getUserInfo(access_token,code);
//            if(CorpUserBaseInfo != null && CorpUserBaseInfo.getUserid() != null){
//                CorpUserDetail user = UserHelper.getUser(access_token,CorpUserBaseInfo.getUserid());
//                Map<String,Object> mapUser = new HashMap<String,Object>();
//                mapUser.put("userId",user.getUserid());
//                mapUser.put("department",user.getDepartment());
//                mapUser.put("mobile",user.getMobile());
//                mapUser.put("email",user.getEmail());
//                mapUser.put("isHide",user.getIsHide());
//                mapUser.put("jobnumber",user.getJobnumber());
//                mapUser.put("avatar",user.getAvatar());
//                mapUser.put("isAdmin",user.getIsAdmin());
//                mapUser.put("name",user.getName());
//                map.put("user",mapUser);
//            }
//        }catch (Exception e){
//            throw new Exception("错了");
//        }
//        return map;
//    }
//
//}
