package com.service;

import com.dao.IMaterialDao;
import com.dao.LogDao;
import com.system.BaseDao;
import com.system.DingUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.*;

@Component
public class MaterialService extends BaseDao {

    @Autowired
    private IMaterialDao materialDao;
    @Autowired
    private LogDao logDao ;

    /**
     * 根据 orgId 获取物料信息
     *
     * @param orgId
     * @param materialTypeId
     * @param materialName
     * @param start
     * @param limit
     * @return
     */
    public Map<String, Object> getMaterialListByOrgId(String orgId, String materialTypeId, String materialName, Integer start, Integer limit) {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            List<Object> resultList = new ArrayList<Object>();
            List<Object> findList = materialDao.getMaterialListByObj(materialTypeId, materialName, start, limit);
            Integer findCount = materialDao.queryCountMaterialListByObj(materialTypeId, materialName);
            for (Object obj : findList) {
                Map finMap = (Map) obj;
                String code = (String) finMap.get("CODE");
                List<Object> imgList = materialDao.queryMaterialAndImgsListOneByCode(code);
                if (imgList.size() > 0) {
                    finMap.put("imgList", imgList);
                }
                resultList.add(finMap);
            }
            map.put("code", 100);
            map.put("data", resultList);
            map.put("count",findCount);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("data", e.getMessage());
        }
        return map;

    }


    /**
     * 根据组织id 获取物料分类
     *
     * @param orgId
     * @return
     */
    public Map<String, Object> getMaterialTypeIdByOrgId(String orgId, String typeName) {
        Map<String, Object> map = new HashMap<String, Object>();
        String sql = "SELECT name,code,pk_marbasclass materialTypeId,pk_org orgId FROM marbasclass_view WHERE pk_org = '" + orgId + "'";
        if (!StringUtils.isEmpty(typeName)) {
            sql += " AND name like '%" + typeName + "%'";
        }
        try {
            List<Object> materialList = this.getDataBySql(sql);
            map.put("code", 100);
            map.put("data", materialList);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("data", e.getMessage());
        }
        return map;
    }

    /**
     * 根据 组织id 和 物料编码 查询物料信息
     *
     * @param code
     * @return
     */
    public Map<String, Object> getMaterialByOrgIdAndCode(String code) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            List<Object> resultList = new ArrayList<Object>();
            List<Object> materialList = materialDao.getMaterialByOrgIdAndCode(code);
            List<Object> imgList = materialDao.queryMaterialAndImgsListOneByCode(code);
            for (Object obj : materialList) {
                Map resultMap = (Map) obj;
                if (imgList.size() > 0) {
                    resultMap.put("imgList", imgList);
                }
                resultList.add(resultMap);
            }
            map.put("code", 100);
            map.put("data", resultList);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("data", e.getMessage());
        }
        return map;
    }

    /**
     * 根据 物料编码 物料主键 维护图片
     *
     * @param code
     * @param userId
     * @param jobNumber
     * @param imgs
     * @return
     */
    public Map<String, Object> subMaterialImgsDataByOrgIdCodeAndId(String code, String userId, String jobNumber, List<Object> imgs) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            insertImgs(code, imgs);
            map.put("code", 100);
            logDao.logInfo("维护图片信息",null,userId);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("data", e.getMessage());
        }
        return map;
    }

    //根据物料 主键 添加图片List
    private void insertImgs(String code, List<Object> imgList) throws Exception {
        try {
            for (Object imgUrl : imgList) {
                String url = (String) imgUrl;
                materialDao.insertImgs(code, url);
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
    }


    /*****************************************************************************************************************************************************/
    /***************************************************************PC端**********************************************************************************/
    /*****************************************************************************************************************************************************/

    /**
     * PC端 物料手册查询
     *
     * @param status
     * @param materialName
     * @param start
     * @param limit
     */
    public Map<String, Object> getMaterialListByPc(String status, String materialName, Integer start, Integer limit) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            List<Object> materialList = null;
            if (status.equals("todo")) {
                Map resultMap = new HashMap();
                materialList = materialDao.queryTodoMaterial(materialName,start, limit);
                Integer findCount = materialDao.queryCountMaterial("todo",materialName);
                resultMap.put("materialList",materialList);
                map.put("data", resultMap);
                map.put("count",findCount);
            }
            if (status.equals("done")) {
                materialList = materialDao.queryDoneMaterialListByDistinctCode(start,limit);
                Integer findCount = materialDao.queryCountMaterial("done",materialName);
                List resultList = new ArrayList();
                for(Object findMaterial : materialList){
                    Map resultMap = new HashMap();
                    Map findMap = (Map) findMaterial;
                    List<Object> findImgs = materialDao.queryMaterialImgsListByCode((String)findMap.get("CODE"));
                    resultMap.put("materialList",findMap);
                    resultMap.put("imgs",findImgs);
                    resultList.add(resultMap);
                }
                map.put("data", resultList);
                map.put("count",findCount);
            }
            map.put("code", 100);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("data", e.getMessage());
        }
        return map;
    }

    /**
     * PC端 同步物料
     *
     * @return
     */
    public Map<String, Object> syncMaterialOrclForMysql(String userId) {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            List<Object> queryList = materialDao.queryAllMaterialListByOrcl();
            List<Object> querySyncList = materialDao.querySyncMaterialForMysql();
            if (querySyncList.size() > 0) {//先清空物料表结构
                materialDao.truncateSyncMaterial();
            }
            List<Object[]> insertList = new ArrayList<Object[]>();
            for (Object obj : queryList) {
                Map materialMap = (Map) obj;
                /**
                 * (UNIT,NAME,PK_MATERIAL,MATERIALTYPE,PK_MARBASCLASS,MATERIALSPEC,CODE,CREATEDATE)
                 */
                Object[] codes = {
                        (String) materialMap.get("UNIT"),
                        (String) materialMap.get("NAME"),
                        (String) materialMap.get("PK_MATERIAL"),
                        (String) materialMap.get("MATERIALTYPE"),
                        (String) materialMap.get("PK_MARBASCLASS"),
                        (String) materialMap.get("MATERIALSPEC"),
                        (String) materialMap.get("CODE"),
                        timestamp
                };
                insertList.add(codes);
            }
            //05556651101293078 gjwork userId
            int[] ints = materialDao.batchInsertMaterialForMysql(insertList);
            DingUtils.sendMessage(userId,"物料同步完成");
            map.put("data", ints);
            map.put("code", 100);
            logDao.logInfo("同步物料",null,userId);
        } catch (Exception e) {
            map.put("errMsg", e.getMessage());
        }
        return map;
    }
}
