package com.dao;

import java.util.List;

public interface IPurchaseDao {

    //根据组织id（orgId）获取需求部门列表
    List<Object> getDeptListByOrgId(String orgId) throws Exception;

    //根据组织Id（orgId） 获取采购组织列表
    List<Object> getPurchaseOrgListByOrgId(String orgId) throws Exception;

    //库存组织列表
    List<Object> getStockOrgListByOrgId(String orgId) throws Exception;

    //获取请购类型列表
    List<Object> getBillTypeList() throws Exception;

    //提交物料需求信息 mysql
    void subPurchaseByObj(String userId, String userName, String jobNumber, String pk_stockorg, String ctrantypeid, String vmemo, String ordernumber) throws Exception;

    void insertPurchaseDetail(String pk_purchaseorg, String pk_material, String castunitid, String nastnum, String dreqdate, String pk_reqdept, String ordernumber) throws Exception;

    List<Object> getPcPurchaseList(Integer start, Integer limit, Integer status, String userName) throws Exception;

    List<Object> findDetailByOrderNum(String ordernumber) throws Exception;

    List<Object> getFrPurchaseZeroByOrderNum(String ordernumber) throws Exception;

    List<Object> getPurchaseDetailByOrderNumber(String ordernumber) throws Exception;

    //物料审批已通过
    void adoptPurchaseSetDate(Integer status, String jobNumber, String ordernumber) throws Exception;

    //拒绝
    void refuseFrPurchaseByOrderNumber(String ordernumber, Integer status, String message) throws Exception;

    //根据主键 查询 fr_purchase
    List<Object> queryPurchaseById(String ordernumber) throws Exception;

    //根据ID 修改请购信息详情
    void editPurchaseByOrderNumber(String userId, String userName, String jobNumber, String pk_stockorg, String ctrantypeid, String vmemo, String ordernumber) throws Exception;
    void updatePurchase_detailByOrderNumber(String pk_purchaseorg, String pk_material, String castunitid, String nastnum, String dreqdate, String pk_reqdept, String ordernumber) throws Exception;;

    Integer queryPcPurchaseCount(Integer status, String userName) throws Exception;
}
