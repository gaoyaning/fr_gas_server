package com.test;

import com.system.BaseDao;
import com.dao.LogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TestService extends BaseDao {

    @Autowired
    private LogDao logDao;

    public List getValue (){
        List list;
        String sql = "SELECT * FROM sm_user_view where cuserid = '1001A11000000015OFGO'";
        String name = "1001A11000000015OFGO";
        String orgId = "0001A1100000000012WE";
        try {
            SqlRowSet rs =  this.orclJdbc.queryForRowSet("SELECT name,code,pk_marbasclass materialTypeId,pk_org orgId FROM bd_marbasclass_view WHERE pk_org = '"+ orgId +"'");
            list = this.getData(rs);
        }catch (Exception e){
            list = new ArrayList();
            list.add(e.getMessage());
        }
        return list;
    }

    public List queryTest() {
        String sql = "select * from material_imgs where id = 90";
        String orgId = "1222";
        String code = "1222";
        String userId = "1222";
        String jobNumber = "333";
        List list;
        try {
            return this.getDataBySqlForMysql(sql);
        }catch (Exception e){
            list = new ArrayList();
            list.add(e.getMessage());
            return list;
        }
//        return null;
    }

    public Integer queryCount() {
        int num = 0;
        try {
            num = this.queryCountForMysql("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num;
    }

    public void logInfo() {
        try {
            logDao.logInfo("测试数据","userName","userId");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
