package com.dao;

import com.system.BaseDao;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class LogDao extends BaseDao {

    public void logInfo(String msg, String userName, String userId) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "insert into sys_log (msg,userName,userId,createDate,updateDate) values('" + msg + "','" + userName + "','" + userId + "','" + timestamp + "','" + timestamp + "')";
        this.updateForMysql(sql);
    }

    public List<Object> getLogList(Integer start, Integer limit) throws Exception {
        String sql = "select * from sys_log where 1 = 1 order by createDate desc limit " + start + "," + limit + "";
        return this.getDataBySqlForMysql(sql);
    }

    public Map<String, Object> getLogCount() throws Exception {
        String sql = "select count(*) count from sys_log ";
        return this.mysqlJdbc.queryForMap(sql);
    }
}
