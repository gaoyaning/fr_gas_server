package com.nc.purchase;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PurchaseWebService {


    /*public static void main(String[] args) {
        PraybillItf server = new PraybillItf();
        PraybillItfPortType port = server.getPraybillItfSOAP11PortHttp();

        Map detail = new HashMap<String, Object>();
        detail.put("pk_purchaseorg_v", "0001A1100000000036AO");
        detail.put("pk_material", "1001A110000000000X8K");
        detail.put("castunitid", "1001A110000000000WMG");
        detail.put("nastnum", "102.00000000");
        detail.put("dreqdate", "2017-11-15 23:59:59");
        detail.put("pk_reqdept_v", "0001A11000000001Y181");
        detail.put("vbmemo", "请购单明细");
        List detailList = new ArrayList();
        detailList.add(detail);

        Map headinfo = new HashMap();
        headinfo.put("pk_org", "0001A1100000000012WE");
        headinfo.put("ctrantypeid", "1001A11000000015L15H");
        headinfo.put("creator", "1001A110000000001IW9");
        headinfo.put("creationtime", "2017-11-15 23:59:59");
        headinfo.put("vmemo", "请购单");
        headinfo.put("ordernumber", "Pray123456");
        headinfo.put("detail", detailList);

        List headinfoList = new ArrayList();
        headinfoList.add(headinfo);
        Map map = new HashMap();

        map.put("headinfo", headinfoList);


        String str = JSONObject.toJSONString(map);
        System.out.println(str);
        String strs = port.praybill(str);
        JSONObject object = JSONObject.parseObject(str);
        Object obj = object.get("headinfo");


    }*/

    public Object ws (){

        PraybillItf server = new PraybillItf();
        PraybillItfPortType port = server.getPraybillItfSOAP11PortHttp();

        Map detail = new HashMap<String, Object>();
        detail.put("pk_purchaseorg", "0001A1100000000036BU");
        detail.put("pk_material", "1001A110000000000X8O");
        detail.put("castunitid", "1001A110000000000WMG");
        detail.put("nastnum", "1122.00000000");
        detail.put("dreqdate", "2017-12-25 23:00:01");
        detail.put("pk_reqdept", "0001A11000000001Y185");
        detail.put("vbmemo", "请购单明细");
        List detailList = new ArrayList();
        detailList.add(detail);

        Map headinfo = new HashMap();
        headinfo.put("pk_stockorg", "0001A1100000000036AO");
        headinfo.put("ctrantypeid", "0001A110000000002OIU");
        headinfo.put("creator", "1001ZZ1000000017AEMK");
        headinfo.put("creationtime", "2017-12-25 23:59:59");
        headinfo.put("vmemo", "请购单");
        headinfo.put("ordernumber", "Pray123456");
        headinfo.put("detail", detailList);

        List headinfoList = new ArrayList();
        headinfoList.add(headinfo);
        Map map = new HashMap();

        map.put("headinfo", headinfoList);


        String str = JSONObject.toJSONString(map);
        System.out.println(str);
        String strs = port.praybill(str);
        JSONObject object = JSONObject.parseObject(strs);
        return object;
    }

    /**
     * webservice 发起请购
     * @return
     */
    public JSONObject startPruchase (Map<String,Object> obj){
        PraybillItf server = new PraybillItf();
        PraybillItfPortType port = server.getPraybillItfSOAP11PortHttp();
        String str = JSONObject.toJSONString(obj);
        String result = port.praybill(str);
        JSONObject object = JSONObject.parseObject(result);
        return object;

    }

}
