package com.controller;

import com.service.PurchaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//发起请购
@Controller
@RequestMapping("/api")
public class PurchaseCtr {

    @Autowired
    private PurchaseService purchaseService;


    /**
     * 根据组织id（orgId）获取需求部门列表
     * @return
     */
    @RequestMapping(value = "/purchase/getDeptListByOrgId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getDeptListByOrgId(/*@RequestBody Map<String,Object> res*/) {

        Map<String, Object> map;
        String orgId = "0001A11000000001XUJM";
        if (StringUtils.isEmpty(orgId)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "ORGID不能为空");
            return map;
        }
        return purchaseService.getDeptListByOrgId(orgId);
    }

    /**
     * 根据组织Id（orgId） 获取采购组织列表
     *
     * @return
     */
    @RequestMapping(value = "/purchase/getPurchaseOrgListByOrgId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getPurchaseOrgListByOrgId(/*@RequestBody Map<String,Object> res*/) {

        Map<String, Object> map;
        String orgId = "0001A1100000000012WE";
        if (StringUtils.isEmpty(orgId)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "ORGID不能为空");
            return map;
        }
        return purchaseService.getPurchaseOrgListByOrgId(orgId);
    }

    /**
     * 根据组织Id（orgId） 获取库存组织列表
     *
     * @return
     */
    @RequestMapping(value = "/purchase/getStockOrgListByOrgId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getStockOrgListByOrgId(/*@RequestBody Map<String,Object> res*/) {

        Map<String, Object> map;
        String orgId = "0001A1100000000012WE";
        if (StringUtils.isEmpty(orgId)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "ORGID不能为空");
            return map;
        }
        return purchaseService.getStockOrgListByOrgId(orgId);
    }

    /**
     * 获取请购类型列表
     *
     * @return
     */
    @RequestMapping(value = "/purchase/getBillTypeList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getBillTypeList() {
        return purchaseService.getBillTypeList();
    }

    /**
     * 提交物料需求信息
     *
     * @return
     */
    @RequestMapping(value = "/purchase/subPurchaseByObj", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> subPurchaseByObj(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();

        //公共信息  请购类型主键(ctrantypeid)，库存组织(pk_org)、jobNumber、userId、userName、
        Map<String, Object> infoMap = (Map<String, Object>) res.get("info");
        String userId = (String) infoMap.get("userId");
        String userName = (String) infoMap.get("userName");
        String jobNumber = (String) infoMap.get("jobNumber");
        String pk_stockorg = (String) infoMap.get("pk_stockorg");
        String ctrantypeid = (String) infoMap.get("ctrantypeid");
        String vmemo = (String) infoMap.get("vmemo");

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(userName) || StringUtils.isEmpty(jobNumber) || StringUtils.isEmpty(pk_stockorg) || StringUtils.isEmpty(ctrantypeid) || StringUtils.isEmpty(vmemo)) {
            map.put("code", 101);
            map.put("errMsg", "缺少必要公共参数");
            return map;
        }

        //获取物料列表
        List<Map<String, String>> detailList = (List<Map<String, String>>) res.get("detail");
        if (detailList == null || detailList.size() == 0) {
            map.put("code", 101);
            map.put("errMsg", "detail 为空");
            return map;
        }

        return purchaseService.subPurchaseByObj(userId, userName, jobNumber, pk_stockorg, ctrantypeid, vmemo, detailList);
    }

    /***************************************************************************************************************************************************************/
    /****************************************************************************PC端查看***************************************************************************/
    /***************************************************************************************************************************************************************/

    /**
     * PC端管理员查询移动端提交的请购
     *
     * @param res
     * @return
     */
    @RequestMapping(value = "/purchase/getPcPurchaseList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getPcPurchaseList(@RequestBody Map<String, Object> res) {
        Integer start = (Integer) res.get("start");
        Integer limit = (Integer) res.get("limit");
        Integer status = (Integer) res.get("status");
        String userName = (String) res.get("userName");
        return purchaseService.getPcPurchaseList(start, limit, status, userName);
    }

    /**
     * PC端审核操作
     */
    @RequestMapping(value = "/purchase/examinePurchase", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> examinePurchase(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String ,Object>();
        String ordernumber = (String) res.get("ordernumber");
        Integer status = (Integer) res.get("status");
        String message = (String) res.get("message");
        String jobNumber  = (String) res.get("jobNumber");
        if(status == null){
            map.put("code",101);
            map.put("errMsg","status为空");
            return map;
        }
        if(StringUtils.isEmpty(ordernumber)){
            map.put("code",101);
            map.put("errMsg","ordernumber为空");
            return map;
        }
        if(StringUtils.isEmpty(jobNumber)){
            map.put("code",101);
            map.put("errMsg","工号为空");
            return map;
        }
        return purchaseService.examinePurchase(ordernumber, status ,message ,jobNumber);
    }

    /*****************************************************************************************/

    /**
     *  PC端 根据ordernumber 流水号 修改 移动端提交的数据
     * @param res
     * @return
     */
    @RequestMapping(value = "/purchase/editPurchaseByObj", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> editPurchaseByObj(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();

        //公共信息  请购类型主键(ctrantypeid)，库存组织(pk_org)、jobNumber、userId、userName、
        Map<String, Object> infoMap = (Map<String, Object>) res.get("info");
        String userId = (String) infoMap.get("userId");
        String userName = (String) infoMap.get("userName");
        String jobNumber = (String) infoMap.get("jobNumber");
        String pk_stockorg = (String) infoMap.get("pk_stockorg");
        String ctrantypeid = (String) infoMap.get("ctrantypeid");
        String vmemo = (String) infoMap.get("vmemo");
        String ordernumber = (String) infoMap.get("ordernumber");

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(userName) || StringUtils.isEmpty(jobNumber) || StringUtils.isEmpty(pk_stockorg) || StringUtils.isEmpty(ctrantypeid) || StringUtils.isEmpty(vmemo)) {
            map.put("code", 101);
            map.put("errMsg", "缺少必要公共参数");
            return map;
        }

        //获取物料列表
        List<Map<String, String>> detailList = (List<Map<String, String>>) res.get("detail");
        if (detailList == null || detailList.size() == 0) {
            map.put("code", 101);
            map.put("errMsg", "detail 为空");
            return map;
        }

        return purchaseService.editPurchaseByObj(ordernumber,userId, userName, jobNumber, pk_stockorg, ctrantypeid, vmemo, detailList);
    }

}
