package com.dao;

import com.system.BaseDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class PurchaseDaoImpl extends BaseDao implements IPurchaseDao {


    @Override
    public List<Object> getDeptListByOrgId(String orgId) throws Exception {
        String sql = "SELECT name,code,pk_dept deptId,pk_org orgId FROM org_dept_view WHERE pk_org = '" + orgId + "'";
        List<Object> list = this.getDataBySql(sql);
        return list;
    }

    @Override
    public List<Object> getPurchaseOrgListByOrgId(String orgId) throws Exception {
        String sql = "SELECT name,code,pk_purchaseorg purChaseOrgId,pk_org orgId FROM org_purchaseorg_view WHERE pk_org = '" + orgId + "'";
        List<Object> list = this.getDataBySql(sql);
        return list;
    }

    @Override
    public List<Object> getStockOrgListByOrgId(String orgId) throws Exception {
        String sql = "SELECT name,code,pk_stockorg stockOrgId,pk_org orgId FROM org_stockorg_view WHERE pk_org = '" + orgId + "'";
        List<Object> list = this.getDataBySql(sql);
        return list;
    }

    @Override
    public List<Object> getBillTypeList() throws Exception {
        String sql = "SELECT pk_billtypecode,billtypename,pk_billtypeid FROM bd_billtype_view";
        List<Object> list = this.getDataBySql(sql);
        return list;
    }

    @Override
    public void subPurchaseByObj(String userId, String userName, String jobNumber, String pk_stockorg, String ctrantypeid, String vmemo, String ordernumber) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "insert into fr_purchase (userId,userName,jobNumber,pk_stockorg,ctrantypeid,vmemo,ordernumber,status,createDate) values(" +
                "'" + userId + "','" + userName + "','" + jobNumber + "','" + pk_stockorg + "','" + ctrantypeid + "','" + vmemo + "','" + ordernumber + "'," + 0 + ",'" + timestamp + "')";
        this.updateForMysql(sql);
    }

    @Override
    public void insertPurchaseDetail(String pk_purchaseorg, String pk_material, String castunitid, String nastnum, String dreqdate, String pk_reqdept, String ordernumber) throws Exception {
        String sql = "insert into purchase_detail (pk_purchaseorg,pk_material,castunitid,nastnum,dreqdate,pk_reqdept,ordernumber) values(" +
                "'" + pk_purchaseorg + "','" + pk_material + "','" + castunitid + "','" + nastnum + "','" + dreqdate + "','" + pk_reqdept + "','" + ordernumber + "')";
        this.updateForMysql(sql);
    }

    @Override
    public List<Object> getPcPurchaseList(Integer start, Integer limit, Integer status, String userName) throws Exception {
        String sql = "select * from fr_purchase where 1 = 1 ";
        if (status != null) {
            sql += " and status = " + status + "";
        }
        if (!StringUtils.isEmpty(userName)) {
            sql += " and userName like '%" + userName + "%'";
        }
        sql += " order by status desc limit " + start + "," + limit + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public List<Object> findDetailByOrderNum(String ordernumber) throws Exception {
        String sql = "select * from purchase_detail where ordernumber = '" + ordernumber + "'";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public List<Object> getFrPurchaseZeroByOrderNum(String ordernumber) throws Exception {
        String sql = "select * from fr_purchase where status = 0 and ordernumber = '" + ordernumber + "'";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public List<Object> getPurchaseDetailByOrderNumber(String ordernumber) throws Exception {
        String sql = "select * from purchase_detail where ordernumber = '" + ordernumber + "'";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public void adoptPurchaseSetDate(Integer status, String jobNumber, String ordernumber) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String updateSql = "update fr_purchase set updateDate = '" + timestamp + "',status = " + status + ",creator = '" + jobNumber + "',creationtime = '" + timestamp + "' where ordernumber = '" + ordernumber + "'";
        this.updateForMysql(updateSql);
    }

    @Override
    public void refuseFrPurchaseByOrderNumber(String ordernumber, Integer status, String message) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String refuseSql = "update fr_purchase set updateDate = '" + timestamp + "',status = " + status + " and message = '" + message + "' where ordernumber = '" + ordernumber + "'";
        this.updateForMysql(refuseSql);
    }

    @Override
    public List<Object> queryPurchaseById(String ordernumber) throws Exception {
        String sql = "select * from fr_purchase as a where a.ordernumber = '" + ordernumber + "'";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public void editPurchaseByOrderNumber(String userId, String userName, String jobNumber, String pk_stockorg, String ctrantypeid, String vmemo, String ordernumber) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "update fr_purchase set pk_stockorg = '" + pk_stockorg + "',ctrantypeid = '" + ctrantypeid + "',vmemo = '" + vmemo + "'" +
                "userId = '" + userId + "',userName = '" + userName + "',jobNumber = '" + jobNumber + "',updateDate = '" + timestamp + "' where ordernumber = '" + ordernumber + "'";
        this.updateForMysql(sql);
    }

    @Override
    public void updatePurchase_detailByOrderNumber(String pk_purchaseorg, String pk_material, String castunitid, String nastnum, String dreqdate, String pk_reqdept, String ordernumber) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "update purchase_detail set pk_purchaseorg = '" + pk_purchaseorg + "',pk_material = '" + pk_material + "',castunitid = '" + castunitid + "',nastnum = " + nastnum + "," +
                " dreqdate = '" + dreqdate + "',pk_reqdept = '" + pk_reqdept + "',updateDate = '" + timestamp + "' where ordernumber = '" + ordernumber + "'";

    }

    @Override
    public Integer queryPcPurchaseCount(Integer status, String userName) throws Exception {
        String sql = "select count(*) from fr_purchase where 1 = 1 ";
        if (status != null) {
            sql += " and status = " + status + "";
        }
        if (!StringUtils.isEmpty(userName)) {
            sql += " and userName like '%" + userName + "%'";
        }
        Integer count = this.queryCountForMysql(sql);
        return count;
    }
}
