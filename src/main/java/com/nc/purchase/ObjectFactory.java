
package com.nc.purchase;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.purchase package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PraybillString_QNAME = new QName("", "string");
    private final static QName _PraybillResponseReturn_QNAME = new QName("", "return");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.purchase
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Praybill }
     * 
     */
    public Praybill createPraybill() {
        return new Praybill();
    }

    /**
     * Create an instance of {@link PraybillResponse }
     * 
     */
    public PraybillResponse createPraybillResponse() {
        return new PraybillResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "string", scope = Praybill.class)
    public JAXBElement<String> createPraybillString(String value) {
        return new JAXBElement<String>(_PraybillString_QNAME, String.class, Praybill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = PraybillResponse.class)
    public JAXBElement<String> createPraybillResponseReturn(String value) {
        return new JAXBElement<String>(_PraybillResponseReturn_QNAME, String.class, PraybillResponse.class, value);
    }

}
