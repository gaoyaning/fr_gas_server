package com.test;

import com.nc.purchase.PurchaseWebService;
import com.system.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.List;

@Controller
//@RequestMapping("/api")
public class TestSqlCtr {

    @Autowired
    private TestService testService;

    @RequestMapping(value = "/test/getValue",method = RequestMethod.GET,produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Object getValue(){

//        return testService.getValue();
//        return testService.queryTest();
        return testService.queryCount();
    }

    public static void main(String[] args) {
    }

    @RequestMapping(value = "/test/ws",method = RequestMethod.GET,produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Object ws(){

        PurchaseWebService test = new PurchaseWebService();
        Object obj = test.ws();
        return obj;
    }

    @RequestMapping(value = "/test/log",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    @ResponseBody
    public void log1(HttpServletRequest request){
        UserSession session = (UserSession) request.getSession().getAttribute("userSession");
        String userName = session.userName;
        String userId = session.userId;
        String jobNumber = session.jobNumber;
        String avatar = session.avatar;
        int num = 0;
        String name = "12";
//        testService.logInfo();
    }


    @RequestMapping(value = "/token",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    @ResponseBody
    public void login(HttpServletRequest request){
        String grant_type = request.getParameter("grant_type");
        String UserName = request.getParameter("UserName");
        String Password = request.getParameter("Password");
    }

}
