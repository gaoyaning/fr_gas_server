package com.system;

import com.alibaba.dingtalk.openapi.Env;
import com.alibaba.dingtalk.openapi.auth.AuthHelper;
import com.alibaba.dingtalk.openapi.message.LightAppMessageDelivery;
import com.alibaba.dingtalk.openapi.message.MessageHelper;
import com.dingtalk.open.client.api.model.corp.MessageBody;
import com.dingtalk.open.client.api.model.corp.MessageType;

//钉钉 工具
public class DingUtils {

    public static void sendMessage(String userId, String msg) throws Exception {

        // 获取access token
        String accessToken = AuthHelper.getAccessToken();
        // 发送微应用消息
        String toUsers = userId;
        String toParties = null;
        String agentId = Env.AGENT_ID + "";
        LightAppMessageDelivery lightAppMessageDelivery = new LightAppMessageDelivery(toUsers, toParties, agentId);

        MessageBody.TextBody textBody = new MessageBody.TextBody();
        textBody.setContent(msg);

        lightAppMessageDelivery.withMessage(MessageType.TEXT, textBody);
        MessageHelper.send(accessToken, lightAppMessageDelivery);
        System.out.println("成功发送 微应用文本消息");
    }

}
