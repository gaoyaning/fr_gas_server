package com.controller;

import com.dao.LogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//其他
@Controller
@RequestMapping("/api")
public class CommonCtr {

    @Autowired
    private LogDao logDao;


    /**
     * 获取 log 日志
     *
     * @return
     */
    @RequestMapping(value = "/log/getLogList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getLogList(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        Integer start = (Integer) res.get("start");
        Integer limit = (Integer) res.get("limit");
        try {
            List<Object> queryList = logDao.getLogList(start, limit);
            Map<String, Object> logCount = logDao.getLogCount();
            map.put("code", 100);
            map.put("data", queryList);
            map.put("count", logCount);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

}
