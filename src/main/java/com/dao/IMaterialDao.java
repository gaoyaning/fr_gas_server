package com.dao;

import java.util.List;

public interface IMaterialDao {

    //根据mysql 查询的code 列表 not in 物料信息
    public List<Object> getMaterialListByOrclNotInCode(String materialTypeId, String materialName, Integer start, Integer limit) throws Exception;

    //根据 materialTypeId materialName 条件查询
    public List<Object> getMaterialListByObj(String materialTypeId, String materialName, Integer start, Integer limit) throws Exception;

    //根据 物料的code值 判断本地MySQL 是否存在，如果有则返回物料信息图片
    public List<Object> queryMaterialAndImgsListOneByCode(String code) throws Exception;

    //根据 组织id 和 物料编码 查询物料信息
    public List<Object> getMaterialByOrgIdAndCode(String code) throws Exception;

    //查询oracle所有物料信息 分页操作
    public List<Object> getMaterialList(Integer start, Integer limit, String materialName) throws Exception;

    //查询oracle所有物料信息 分页操作 not in
    public List<Object> getMaterialListCodeNotIn(Integer start, Integer limit, String materialName, String codeList) throws Exception;

    //查询oracle 所有物料信息
    public List<Object> queryAllMaterialListByOrcl() throws Exception;

    //查询 mysql 里面是否有同步数据
    List<Object> querySyncMaterialForMysql() throws Exception;

    //清空 同步物料表数据
    void truncateSyncMaterial() throws Exception;

    //批量添加
    int[] batchInsertMaterialForMysql(List<Object[]> queryList);

    //查询未维护得物料列表
    List<Object> queryTodoMaterial(String materialName, Integer start, Integer limit) throws Exception;

    //查询 以维护得物料 列表
    List<Object> queryDoneMaterial(String materialName, Integer start, Integer limit) throws Exception;

    //查询以维护的物料  中间表 去重
    List<Object> queryDoneMaterialListByDistinctCode(Integer start, Integer limit) throws Exception;

    //根据code 查询图片列表
    List<Object> queryMaterialImgsListByCode(String code) throws Exception;

    //根据物料 主键 添加图片List
    void insertImgs(String code, String imgUrl) throws Exception;

    Integer queryCountMaterial(String state, String materialName) throws Exception;

    Integer queryCountMaterialListByObj(String materialTypeId, String materialName) throws Exception;
}
