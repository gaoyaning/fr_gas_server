package com.controller;

import com.service.OrgService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

//组织
@Controller
@RequestMapping("/api")
public class OrgCtr {

    @Autowired
    private OrgService orgService;


    /**
     * 根据工号 获取当前组织列表
     *
     * @return
     */
    @RequestMapping(value = "/org/getOrgOrgsByJobnumber", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getOrgOrgsByJobnumber(/*@RequestBody Map<String, Object> res*/) {

        Map<String, Object> map;
//        String jobNumber = (String) res.get("jobNumber");
        String jobNumber = "1001A11000000015OFGO";
        if (StringUtils.isEmpty(jobNumber)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "工号不能为空");
            return map;
        }
        map = orgService.getOrgOrgsByJobnumber(jobNumber);

        return map;
    }

}
