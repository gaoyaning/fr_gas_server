package com.system;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 初始化 执行
 */
@Component
public class InitExecute implements CommandLineRunner{

    @Override
    public void run(String... strings) throws Exception {
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<项目开始启动>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
}
