package com.controller;

import com.service.MaterialService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Materiel:物料 Manual:手册
@Controller
@RequestMapping("/api")
public class MaterialManualCtr {

    @Autowired
    private MaterialService materialService;

    /**
     * 根据 组织id 获取物料信息
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/material/getMaterialListByOrgId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getMaterialListByOrgId(@RequestBody Map<String,Object> res) {
        Map<String, Object> map;
//        String orgId = (String)res.get("ORGID");
        String materialTypeId = (String)res.get("materialTypeId");
        String orgId = "0001A1100000000012WE";
        String materialName = (String) res.get("materialName");
        Integer start = (Integer) res.get("start");
        Integer limit = (Integer) res.get("limit");
        if(start == null || limit == null){
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "start || limit => isNull");
            return map;
        }
        return materialService.getMaterialListByOrgId(orgId, materialTypeId, materialName,start,limit);
    }

    /**
     * 根据组织id 获取物料分类列表
     * @return
     */
    @RequestMapping(value = "/material/getMaterialTypeIdByOrgId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getMaterialTypeIdByOrgId(/*@RequestBody Map<String,Object> res*/) {
        Map<String, Object> map;
//        String orgId = (String)res.get("ORGID");
        String orgId = "0001A1100000000012WE";
        String typeName = "材";
        if (StringUtils.isEmpty(orgId)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "ORGID不能为空");
            return map;
        }
        return materialService.getMaterialTypeIdByOrgId(orgId, typeName);
    }

    /**
     * 根据 物料编码 查询物料信息
     */
    @RequestMapping(value = "/material/getMaterialByOrgIdAndCode", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getMaterialByOrgIdAndCode(@RequestBody Map<String,Object> res) {
        Map<String, Object> map;
        String code = (String)res.get("code");
        if (StringUtils.isEmpty(code)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "物料编码不能为空");
            return map;
        }
        return materialService.getMaterialByOrgIdAndCode(code);
    }

    /**
     * 根据 物料编码 维护图片
     */
    @RequestMapping(value = "/material/subMaterialImgsDataByOrgIdCodeAndId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> subMaterialImgsDataByOrgIdCodeAndId(@RequestBody Map<String,Object> res) {
        Map<String, Object> map;
        String code = (String) res.get("code");
        String userId = (String) res.get("userId");
        String jobNumber = (String) res.get("jobNumber");
        List<Object> imgs = (List<Object>) res.get("imgs");
        if (StringUtils.isEmpty(code)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "code不能为空");
            return map;
        }
        if (StringUtils.isEmpty(jobNumber)) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "工号不能为空");
            return map;
        }
        if (imgs == null || imgs.size() == 0 || imgs.size() > 3) {
            map = new HashMap<String, Object>();
            map.put("code", 101);
            map.put("errMsg", "至少上传一张照片,最多不超过3张");
            return map;
        }
        return materialService.subMaterialImgsDataByOrgIdCodeAndId(code, userId, jobNumber, imgs);
    }

    /*****************************************************************************************************************************************************/
    /***************************************************************PC端**********************************************************************************/
    /*****************************************************************************************************************************************************/

    /**
     * PC端 物料手册查询
     * status todo 未维护  done 以维护
     */
    @RequestMapping(value = "/material/getMaterialBListByPc", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getMaterialBListByPc(@RequestBody Map<String,Object> res) {
        Map<String, Object> map = new HashMap<String,Object>();
        String status  = (String) res.get("status");
        String materialName = (String) res.get("materialName");
        Integer start = (Integer) res.get("start");
        Integer limit = (Integer) res.get("limit");
        if(start == null || limit == null){
            map.put("code",101);
            map.put("errMsg","缺少分页");
            return map;
        }
        map = materialService.getMaterialListByPc(status, materialName, start, limit);
        return map;
    }


    /**
     * PC端 同步物料
     * @return
     */
    @RequestMapping(value = "/material/syncMaterialOrclForMysql", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> syncMaterialOrclForMysql(@RequestBody Map<String,Object> res) {
        String userId = (String) res.get("userId");
        Map<String, Object> map = new HashMap<String,Object>();
        if(StringUtils.isEmpty(userId)){
            map.put("code",101);
            map.put("errMsg","userId为空");
            return map;
        }
        return materialService.syncMaterialOrclForMysql(userId);
    }


}
