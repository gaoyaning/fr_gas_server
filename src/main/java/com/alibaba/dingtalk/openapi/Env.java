package com.alibaba.dingtalk.openapi;


/**
 * 企业应用接入时的常量定义
 */
public class Env {

    /**
     * 企业应用接入秘钥相关
     */
    public static final String CORP_ID = "ding4974e26e154775e735c2f4657eb6378f";
    public static final String CORP_SECRET = "8r6kDIcVbjeC9dLi6fyzV6xfnxH3UqSnQZ2hOtXO-Kkiec4csY4RpEG6QrR24NnQ";
    public static final Integer AGENT_ID = 152746965;//113879980
    public static final String PC_DOMAIN = "http://192.168.10.41:8080/";//"http://192.168.10.41:8081/";
    public static final String MOB_DOMAIN = "http://192.168.10.41:8080/";
    public static final String SSO_Secret = "";

    /**
     * DING API地址
     */
	public static final String OAPI_HOST = "https://oapi.dingtalk.com";
    /**
     * 企业应用后台地址，用户管理后台免登使用
     */
	public static final String OA_BACKGROUND_URL = "";


    /**
     * 企业通讯回调加密Token，注册事件回调接口时需要传递给钉钉服务器
     */
	public static final String TOKEN = "";
	public static final String ENCODING_AES_KEY = "";
	
}
