package com.controller;

import com.service.ProductionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//生产任务
@Controller
@RequestMapping("/api")
public class ProductionCtr {

    @Autowired
    private ProductionService productionService;


    /**
     * 查询所有生产列表
     * @param res
     * @return
     */
    @RequestMapping(value = "/production/getProductionList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getProductionList(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        Integer start = (Integer) res.get("start");
        Integer limit = (Integer) res.get("limit");
        if(start == null || limit == null){
            map.put("code", 101);
            map.put("errMsg", "start || limit => isNull");
            return map;
        }
        return productionService.getProductionList(start,limit);
    }

    /**
     * 添加生产任务组
     * @param res
     * @return
     */
    @RequestMapping(value = "/production/addProduct", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> addProduct(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = (String) res.get("userId");
        String userName = (String) res.get("userName");
        String addName = (String) res.get("addName");
        if (StringUtils.isEmpty(userId)) {
            map.put("code", 101);
            map.put("errMsg", "userId为空");
            return map;
        }
        if (StringUtils.isEmpty(userName)) {
            map.put("code", 101);
            map.put("errMsg", "userName为空");
            return map;
        }
        if (StringUtils.isEmpty(addName)) {
            map.put("code", 101);
            map.put("errMsg", "addName为空");
            return map;
        }
        return productionService.addProduct(userId,userName,addName);
    }

    /**
     * 根据生产任务表 主键 proId  修改proName
     * @param res
     * @return
     */
    @RequestMapping(value = "/production/editProNameById", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> editProNameById(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        Integer proId = (Integer) res.get("proId");
        String proName = (String) res.get("proName");
        String userId = (String) res.get("userId");
        String userName = (String) res.get("userName");
        //nameList || proName 至少有一个非空
        if(StringUtils.isEmpty(proName)){
            map.put("code",101);
            map.put("errMsg","proName => 空");
        }
        if(proId == null){
            map.put("code", 101);
            map.put("errMsg", "proId == null");
            return map;
        }
        return productionService.editProNameById(proId,proName);
    }

    /**
     * 根据生产任务表 主键 proId  删除
     * @param res
     * @return
     */
    @RequestMapping(value = "/production/delProductById", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> delProductById(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        Integer proId = (Integer) res.get("proId");
        if(proId == null){
            map.put("code", 101);
            map.put("errMsg", "proId == null");
            return map;
        }
        return productionService.delProductById(proId);
    }

    /**
     * 根据生产任务 主键 proId  添加/修改任务列表
     * @param res
     * @return
     */
    @RequestMapping(value = "/production/addTaskListByProId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> addTaskListByProId(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        Integer proId = (Integer) res.get("proId");
        List<Map> taskList = (List<Map>) res.get("taskList");
        if(proId == null){
            map.put("code", 101);
            map.put("errMsg", "proId == null");
            return map;
        }
        if(taskList == null){
            map.put("code", 101);
            map.put("errMsg", "taskList == null");
            return map;
        }
        return productionService.addTaskListByProId(proId,taskList);
    }

    /*****************************************************************************************************************************************************/
    /***************************************************************任务捆绑*******************************************************************************/
    /*****************************************************************************************************************************************************/

    /**
     * PC端查询任务捆绑列表 分页
     * @param res
     * @return
     */
    @RequestMapping(value = "/production/getBindProList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getBindProList(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        Integer start = (Integer) res.get("start");
        Integer limit = (Integer) res.get("limit");
        if(start == null || limit == null){
            map.put("code", 101);
            map.put("errMsg", "start || limit => isNull");
            return map;
        }
        return productionService.getBindProList(start,limit);
    }

    /**
     *  添加绑定任务
     */
    @RequestMapping(value = "/production/addBindPro", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> addBindPro(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        String company = (String) res.get("company");
        String org = (String) res.get("org");
        String sellOrder = (String) res.get("sellOrder");
        String proOrder = (String) res.get("proOrder");
        Integer proId = (Integer) res.get("proId");
        String userId = (String) res.get("userId");
        if(StringUtils.isEmpty(company) || StringUtils.isEmpty(org) || StringUtils.isEmpty(sellOrder) || StringUtils.isEmpty(proOrder) || StringUtils.isEmpty(userId) || proId == null){
            map.put("code",101);
            map.put("errMsg","参数有误");
            return map;
        }
        return productionService.addBindPro(company,org,sellOrder,proOrder,proId,userId);
    }


    /*****************************************************************************************************************************************************/
    /***************************************************************移动端********************************************************************************/
    /*****************************************************************************************************************************************************/

    /**
     * 根据 任务号 查询 任务
     */
    @RequestMapping(value = "/production/getProDataByOrderNo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> getProDataByOrderNo(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        String orderNo = (String) res.get("orderNo");
        if(StringUtils.isEmpty(orderNo)){
            map.put("code", 101);
            map.put("errMsg", "orderNo => null");
            return map;
        }
        return productionService.getProDataByOrderNo(orderNo);
    }

    /**
     * 生产报功
     */
    @RequestMapping(value = "/production/reportWorkByUser", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Map<String, Object> reportWorkByUser(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        Integer proId = (Integer) res.get("proId");
        Integer taskId = (Integer) res.get("taskId");
        String userId = (String) res.get("userId");
        String userName = (String) res.get("userName");
        String jobNumber = (String) res.get("jobNumber");
        if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(userName) || StringUtils.isEmpty(jobNumber)){
            map.put("code", 101);
            map.put("errMsg", "缺少用户请求参数");
            return map;
        }
        if(proId == null){
            map.put("code", 101);
            map.put("errMsg", "proId == null");
            return map;
        }
        if(taskId == null){
            map.put("code", 101);
            map.put("errMsg", "taskId == null");
            return map;
        }
        return productionService.reportWorkByUser(proId,taskId,userId,userName,jobNumber);
    }


}
