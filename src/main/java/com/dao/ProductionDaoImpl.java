package com.dao;

import com.system.BaseDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class ProductionDaoImpl extends BaseDao implements IProductionDao {


    @Override
    public List<Object> getProductionList(Integer start, Integer limit) throws Exception {
        String sql = "select * from sys_production ORDER BY createDate desc limit " + start + "," + limit + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public Integer getProductionCount() throws Exception {
        String sql = "select count(*) from sys_production";
        return this.queryCountForMysql(sql);
    }

    @Override
    public List<Object> getProductTaskListByProId(Integer proId) throws Exception {
        String sql = "select * from sys_product_task as a where a.proId = " + proId + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public void addSysProduction(String proName, String userId, String userName) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "insert into sys_production (proName,userId,userName,enable,createDate,updateDate) VALUES('" + proName + "','" + userId + "','" + userName + "',0,'" + timestamp + "','" + timestamp + "')";
        this.updateForMysql(sql);
    }

    @Override
    public void editProNameById(Integer proId, String proName) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "update sys_production set proName = '" + proName + "',updateDate = '" + timestamp + "' where proId = " + proId + "";
        this.updateForMysql(sql);
    }

    @Override
    public void delProductById(Integer proId) throws Exception {
        String sql = "delete from sys_production where proId = " + proId + "";
        this.updateForMysql(sql);
    }

    @Override
    public void addTaskListByProId(Integer proId, String taskName, Integer sortNo) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "insert into sys_product_task (proId,sortNo,taskName,status,createDate,updateDate) values(" + proId + "," + sortNo + ",'" + taskName + "','todo','" + timestamp + "','" + timestamp + "')";
        this.updateForMysql(sql);
    }

    @Override
    public List<Object> queryTaskListByProId(Integer proId) throws Exception {
        String sql = "select * from sys_product_task as a where a.proId = " + proId + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public void delTaskListByProId(Integer proId) throws Exception {
        String sql = "DELETE FROM sys_product_task WHERE proId = " + proId + "";
        this.updateForMysql(sql);
    }

    @Override
    public List<Object> queryBindProListByProId(Integer proId) throws Exception {
        String sql = "select * from sys_bind_pro where proId = " + proId + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public List<Object> getBindProList(Integer start, Integer limit) throws Exception {
        String sql = "select * from sys_bind_pro order by createDate desc";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list;
    }

    @Override
    public Integer getBindProCount() throws Exception {
        String sql = "select count(*) from sys_bind_pro";
        Integer count = this.queryCountForMysql(sql);
        return count;
    }

    @Override
    public void addBindPro(String company, String org, String sellOrder, String proOrder, Integer proId, String userId, String orderNo) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "insert into sys_bind_pro (company,org,sellOrder,proOrder,proId,userId,orderNo,createDate,updateDate) values(" +
                "'" + company + "','" + org + "','" + sellOrder + "','" + proOrder + "'," + proId + ",'" + userId + "','" + orderNo + "','" + timestamp + "','" + timestamp + "')";
        this.updateForMysql(sql);
    }

    @Override
    public Map<String, Object> getProDataByOrderNo(String orderNo) throws Exception {
        String sql = "select * from sys_bind_pro as a where a.orderNo = '" + orderNo + "'";
        Map<String, Object> map = this.mysqlJdbc.queryForMap(sql);
        return map;
    }

    @Override
    public Map<String, Object> getProductionById(Integer proId) throws Exception {
        String sql = "select * from sys_production as a where a.proId = " + proId + "";
        Map<String, Object> map = this.mysqlJdbc.queryForMap(sql);
        return map;
    }

    @Override
    public void updateProTaskById(Integer taskId, String userId, String userName, String jobNumber) throws Exception {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String sql = "update sys_product_task set userId = '" + userId + "', userName = '" + userName + "'" +
                ", jobNumber = '" + jobNumber + "',status = 'done',updateDate = '" + timestamp + "' where taskId = " + taskId + "";
        this.updateForMysql(sql);
    }

}
