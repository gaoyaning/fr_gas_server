package com.dao;

import java.util.List;
import java.util.Map;

public interface IProductionDao {

    //查询 所有生产项目组
    List<Object> getProductionList(Integer start, Integer limit) throws Exception;

    Integer getProductionCount() throws Exception;

    //根据 proId 获取 关联的任务列表
    List<Object> getProductTaskListByProId(Integer proId) throws Exception;

    //添加生产项目组
    void addSysProduction(String proName, String userId, String userName) throws Exception;

    //根据 proId 修改nameList
    void editProNameById(Integer proId, String proName) throws Exception;

    //根据 proId 删除
    void delProductById(Integer proId) throws Exception;

    //根据proId  添加 taskName sortNo
    void addTaskListByProId(Integer proId, String taskName, Integer sortNo) throws Exception;

    //根据proId 查询
    List<Object> queryTaskListByProId(Integer proId) throws Exception;

    //根据proId 删除
    void delTaskListByProId(Integer proId) throws Exception;

    //根据proId  查询bindPro
    List<Object> queryBindProListByProId(Integer proId) throws Exception;

    //查询 绑定任务 列表 分页
    List<Object> getBindProList(Integer start, Integer limit) throws Exception;

    //查询绑定任务 的 总数
    Integer getBindProCount() throws Exception;

    //添加绑定任务
    void addBindPro(String company, String org, String sellOrder, String proOrder, Integer proId, String userId,String orderNo) throws Exception;

    //根据 任务号 查询 任务
    Map<String, Object> getProDataByOrderNo(String orderNo) throws Exception;

    //根据proId  查询production => Map
    Map<String,Object> getProductionById(Integer proId) throws Exception;

    //根据 主键 taskId 修改
    void updateProTaskById(Integer taskId, String userId, String userName, String jobNumber) throws Exception;
}
