package com.service;

import com.dao.IProductionDao;
import com.dao.LogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductionService {

    @Autowired
    private IProductionDao productionDao;
    @Autowired
    private LogDao logDao ;

    public Map<String, Object> getProductionList(Integer start, Integer limit) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            List<Object> resultList = new ArrayList<Object>();
            List<Object> findProductList = productionDao.getProductionList(start, limit);
            Integer count = productionDao.getProductionCount();
            for (Object obj : findProductList) {
                Map resultMap = new HashMap();
                Map<String, Object> proMap = (Map<String, Object>) obj;
                List<Object> taskList = productionDao.getProductTaskListByProId(new Integer(proMap.get("proId").toString()));
                resultMap.put("taskList", taskList);
                resultMap.put("production", proMap);
                resultList.add(resultMap);
            }
            map.put("count", count);
            map.put("data", resultList);
            map.put("code", 100);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> addProduct(String userId, String userName, String addName) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            productionDao.addSysProduction(addName, userId, userName);
            map.put("code", 100);
            logDao.logInfo("添加生产任务组",userName,userId);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    //
    public Map<String, Object> editProNameById(Integer proId, String proName) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            productionDao.editProNameById(proId, proName);
            map.put("code", 100);
            logDao.logInfo("添加生产任务表",null,null);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> delProductById(Integer proId) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //查询是否有捆绑任务，如果有则不允许删除
            List<Object> queryBind = productionDao.queryBindProListByProId(proId);
            if(queryBind.size() > 0){
                map.put("code",101);
                map.put("errMsg","当前有捆绑任务。请先删除后在执行此操作");
                return map;
            }
            //先查询是否存在 =》 有则删除
            List<Object> queryList = productionDao.queryTaskListByProId(proId);
            if(queryList.size() > 0){
                productionDao.delTaskListByProId(proId);
            }
            productionDao.delProductById(proId);
            map.put("code", 100);
            logDao.logInfo("删除生产任务",null,null);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> addTaskListByProId(Integer proId, List<Map> taskList) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //先查询是否存在 =》 有则删除
            List<Object> queryList = productionDao.queryTaskListByProId(proId);
            if(queryList.size() > 0){
                productionDao.delTaskListByProId(proId);
            }
            for (Map taskMap : taskList) {
                String sortNo = taskMap.get("sortNo").toString();
                String taskName = (String) taskMap.get("taskName");
                productionDao.addTaskListByProId(proId, taskName, new Integer(sortNo));
            }
            map.put("code", 100);
            logDao.logInfo("修改任务列表",null,null);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    public Map<String,Object> getBindProList(Integer start, Integer limit) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            List<Object> queryBind = productionDao.getBindProList(start,limit);
            Integer count = productionDao.getBindProCount();
            map.put("data",queryBind);
            map.put("count",count);
            map.put("code", 100);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    public Map<String, Object> addBindPro(String company, String org, String sellOrder, String proOrder, Integer proId, String userId) {
        Map<String, Object> map = new HashMap<String, Object>();
        String orderNo = randomOrderNo();
        try {
            productionDao.addBindPro(company,org,sellOrder,proOrder,proId,userId,orderNo);
            map.put("code", 100);
            logDao.logInfo("添加绑定任务",null,userId);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    //生成订单号
    private String randomOrderNo(){
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        // 去掉"-"符号
        String temp = str.substring(0, 8);/* + str.substring(9, 13)
                + str.substring(14, 18) + str.substring(19, 23)
                + str.substring(24);*/
        String s = new Date().getTime() + "";
        return temp + s.substring(8);
    }

    public Map<String, Object> getProDataByOrderNo(String orderNo) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Map<String,Object> findBind = productionDao.getProDataByOrderNo(orderNo);
            Integer proId = (Integer) findBind.get("proId");
            Map<String,Object> findPro = productionDao.getProductionById(proId);
            List<Object> findTask = productionDao.getProductTaskListByProId(proId);
            map.put("production",findPro);
            map.put("task",findTask);
            map.put("code", 100);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }

    //生产报功
    public Map<String, Object> reportWorkByUser(Integer proId, Integer taskId, String userId, String userName, String jobNumber) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            productionDao.updateProTaskById(taskId,userId,userName,jobNumber);
            map.put("code", 100);
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
        }
        return map;
    }
}
