package com.service;

import com.system.BaseDao;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OrgService extends BaseDao {

    public Map<String, Object> getOrgOrgsByJobnumber(String jobNumber) {
        Map<String, Object> map = new HashMap<String, Object>();
        String sql = "SELECT user_code userCode , user_name userName , cuserid userId, pk_org orgId  FROM sm_user_view WHERE cuserid = '" + jobNumber + "'";
        try {
            List<Object> userList = this.getDataBySql(sql);
            if (userList.size() > 0) {
                Map<String, Object> resutlMap = (Map) userList.get(0);
                String orgId = (String) resutlMap.get("ORGID");
                List<Object> orgList = getOrgOrgsById(orgId);
                map.put("code", 100);
                map.put("data", orgList);
            } else {
                map.put("code", 100);
                map.put("data", new Object[]{});
            }
            return map;
        } catch (Exception e) {
            map.put("code", 101);
            map.put("errMsg", e.getMessage());
            return map;
        }
    }

    /**
     * 根据 组织主键 获取组织详情
     *
     * @param orgId
     * @return
     * @throws Exception
     */
    public List<Object> getOrgOrgsById(String orgId) throws Exception {
        String sql = "SELECT CODE code,NAME name,PK_ORG orgId  FROM org_orgs WHERE PK_ORG = '" + orgId + "'";
        try {
            return this.getDataBySql(sql);
        } catch (Exception e) {
            throw new Exception();
        }
    }

}
