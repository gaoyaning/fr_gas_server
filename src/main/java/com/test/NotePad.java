package com.test;

import com.alibaba.dingtalk.openapi.utils.aes.DingTalkEncryptException;
import com.alibaba.dingtalk.openapi.utils.aes.DingTalkEncryptor;
import com.system.DingUtils;
import sun.security.provider.MD5;

import java.util.Date;
import java.util.UUID;

public class NotePad {
    // TODO: 2017/12/24  已完成接口
    //OrgCtr
    //org/getOrgOrgsByJobnumber =》根据工号 获取当前组织列表

    //MaterialManualCtr
    //根据 组织id 获取物料信息   material/getMaterialListByOrgId
    //根据组织id 获取物料分类列表    material/getMaterialTypeIdByOrgId
    //根据 组织id 和 物料编码 查询物料信息   material/getMaterialByOrgIdAndCode
    //根据 组织id 和 物料编码 物料主键 维护图片   material/subMaterialImgsDataByOrgIdCodeAndId

    //PurchaseCtr
    //根据组织id（orgId）获取需求部门列表  purchase/getDeptListByOrgId
    //根据组织Id（orgId） 获取采购组织列表   purchase/getPurchaseOrgListByOrgId
    //根据组织Id（orgId） 获取库存组织列表   purchase/getStockOrgListByOrgId
    //获取请购类型列表   purchase/getBillTypeList
    //提交物料需求信息   purchase/subPurchaseByObj

    // TODO: 2017/12/25 物料手册 批量页面 需要分页查询
    //分页查询语句
    /**
     * select * from (
     *      select a.*,rownum rn from ( select * from bd_material ) a where rownum <= 20
     * ) b
     *      where b.pk_org = 'aaa' and b.name like '%000%' and rn > 10
     */


    /**
     * 自动生成32位的UUid，对应数据库的主键id进行插入用。
     * @return
     */
    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        // 去掉"-"符号
        String temp = str.substring(0, 8);/* + str.substring(9, 13)
                + str.substring(14, 18) + str.substring(19, 23)
                + str.substring(24);*/
        String s = new Date().getTime() + "";

        return temp + s.substring(8);

//        return UUID.randomUUID().toString().replace("-", "");
    }


    public static void main(String[] args) {
    }
}
